Replication package for the paper "Identification of Design Points in Pull Requests" submitted to ECEC/FSE 2017.


1. the main results for Table 5 can be computed by running the source code in "pedictionmodels.PredictionModels.java"

2. The Raw data of the 16 pull requests are in the fold "dataset" (Table 2)

3. To genrate the 19 features as presented in Table 4, please open the package "cross.feature", and run "Step1featureExtraction", "Step2TransferTermstoTF", and "Step3GenerateDatasets"