package predictionmodels;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.RandomForest;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class RandomGuess {
	private static String dir = "output2/";

	public static void main(String[] args) throws Exception {
		List<File> allFiles = getAllfiles("dataset/", "arff");

		ArrayList<String> projNames = new ArrayList<String>();

		for (File a : allFiles) {
			projNames.add(a.getName());
		}

		for (String proj : projNames) {

			Instances TestSet = new DataSource(dir + proj + "_Test.arff").getDataSet();
			TestSet.setClassIndex(TestSet.numAttributes() - 1);
			double positive = 0;
			double negative = 0;
			for (int i = 0; i < TestSet.numInstances(); i++) {
				Instance cur = TestSet.instance(i);

				if (cur.classValue() == 1) {
					positive++;
				} else {
					negative++;
				}

			}
			
			double recall=0.5;
			double precision = positive/(positive+negative);
			double f1 = 2*recall*precision/(precision+recall);
			
			System.out.println(proj+"\t"+precision+"\t"+recall+"\t"+f1);

		}

	}

	public static List<File> getAllfiles(String path, String extension) {
		ArrayList<File> ret = new ArrayList<File>();
		try {
			Files.walk(Paths.get(path)).forEach(filePath -> {
				if (Files.isRegularFile(filePath))
					if (filePath.toString().toLowerCase().endsWith("." + extension))
						ret.add(filePath.toFile());
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
}
